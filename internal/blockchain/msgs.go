package blockchain

import (
	bcproto "bitbucket.org/number571/tendermint/proto/tendermint/blockchain"
	"bitbucket.org/number571/tendermint/types"
)

const (
	MaxMsgSize = types.MaxBlockSizeBytes +
		bcproto.BlockResponseMessagePrefixSize +
		bcproto.BlockResponseMessageFieldKeySize
)
