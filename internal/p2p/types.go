package p2p

import (
	"bitbucket.org/number571/tendermint/internal/p2p/conn"
)

type ChannelDescriptor = conn.ChannelDescriptor
type ConnectionStatus = conn.ConnectionStatus
