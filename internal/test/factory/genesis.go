package factory

import (
	"sort"

	cfg "bitbucket.org/number571/tendermint/config"
	tmtime "bitbucket.org/number571/tendermint/libs/time"
	"bitbucket.org/number571/tendermint/types"
)

func RandGenesisDoc(
	config *cfg.Config,
	numValidators int,
	randPower bool,
	minPower int64) (*types.GenesisDoc, []types.PrivValidator) {

	validators := make([]types.GenesisValidator, numValidators)
	privValidators := make([]types.PrivValidator, numValidators)
	for i := 0; i < numValidators; i++ {
		val, privVal := RandValidator(randPower, minPower)
		validators[i] = types.GenesisValidator{
			PubKey: val.PubKey,
			Power:  val.VotingPower,
		}
		privValidators[i] = privVal
	}
	sort.Sort(types.PrivValidatorsByAddress(privValidators))

	return &types.GenesisDoc{
		GenesisTime:   tmtime.Now(),
		InitialHeight: 1,
		ChainID:       config.ChainID(),
		Validators:    validators,
	}, privValidators
}
