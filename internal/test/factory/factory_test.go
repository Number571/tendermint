package factory

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/number571/tendermint/types"
)

func TestMakeHeader(t *testing.T) {
	_, err := MakeHeader(&types.Header{})
	assert.NoError(t, err)
}
