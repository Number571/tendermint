package checktx

import (
	"context"

	"bitbucket.org/number571/tendermint/abci/example/kvstore"
	"bitbucket.org/number571/tendermint/config"
	"bitbucket.org/number571/tendermint/internal/mempool"
	mempoolv0 "bitbucket.org/number571/tendermint/internal/mempool/v0"
	"bitbucket.org/number571/tendermint/proxy"
)

var mp mempool.Mempool

func init() {
	app := kvstore.NewApplication()
	cc := proxy.NewLocalClientCreator(app)
	appConnMem, _ := cc.NewABCIClient()
	err := appConnMem.Start()
	if err != nil {
		panic(err)
	}

	cfg := config.DefaultMempoolConfig()
	cfg.Broadcast = false

	mp = mempoolv0.NewCListMempool(cfg, appConnMem, 0)
}

func Fuzz(data []byte) int {
	err := mp.CheckTx(context.Background(), data, nil, mempool.TxInfo{})
	if err != nil {
		return 0
	}

	return 1
}
