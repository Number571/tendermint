package commands

import (
	"fmt"

	"github.com/spf13/cobra"

	"bitbucket.org/number571/tendermint/version"
)

// VersionCmd ...
var VersionCmd = &cobra.Command{
	Use:   "version",
	Short: "Show version info",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(version.TMVersion)
	},
}
